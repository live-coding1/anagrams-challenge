def is_anagram(word1, word2):
    # strip and put all words in the lower case and replace all spaces with empty character
    word1 = word1.strip().lower().replace(' ', '')
    word2 = word2.strip().lower().replace(' ', '')

    if word1 == '' or word2 == '':
        return False
    # Sort both words
    if sorted(word1) == sorted(word2):
        return True

    return False

# Test cases

print(is_anagram('', '') == False)

print(is_anagram('Listen', '') == False)

print(is_anagram('', 'Listen') == False)

print(is_anagram('Modern', 'Listen') == False)

print(is_anagram('Silent', 'Listen') == True)

# all is ok now ;)
# please subscribe and share if you like this video !